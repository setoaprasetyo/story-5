from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from . import views
from polls import views as pollsViews
from faq import views as faqViews
from artwork import views as artworkViews
from contact import views as contactViews
from associates import views as associatesViews

urlpatterns = [
    path('polls/', include('polls.urls')),
    url(r'admin/', admin.site.urls),
    url(r'index/$', pollsViews.index),
    url(r'faq/$', faqViews.faq),
    url(r'artwork/$', artworkViews.artwork),
    url(r'contact/$', contactViews.contact),
    url(r'associates/$', associatesViews.associates),
    url(r'^$', views.start),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)