from django.db import models

# Create your models here.
class Friendship(models.Model):
    PILIHAN = (('2001', '2001'),('2002', '2002'),('2003', '2003'),('2004','2004'),
        ('2005','2005'),('2006','2006'),('2007','2007'),('2008','2008'),('2009','2009'),('2010','2010'),
        ('2011','2011'),('2012','2012'),('2013','2013'),('2014','2014'),('2015','2015'),('2016','2016'),
        ('2017','2017'),('2018','2018'),('2019','2019'),('2020','2020'))
    nama = models.CharField(max_length= 100)
    hobby = models.CharField(max_length= 100, default='none')
    food = models.CharField(max_length= 100, default='none')
    tahun = models.TextField(choices=PILIHAN)
    
    published = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama)