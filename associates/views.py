from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ContactForm
from .models import Friendship
# Create your views here.
def associates(request):
    posts = Friendship.objects.all()
    contact_form = ContactForm()
    context = {
        'posts':posts,
    }
    if request.method == 'POST':
        Friendship.objects.create(
        nama = request.POST['nama'],
        tahun = request.POST['tahun'],
        food = request.POST['food'],
        hobby = request.POST['hobby']
        )
        return HttpResponseRedirect("/associates/")

    return render(request,'associates.html', context)